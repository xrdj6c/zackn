### What is this repository for? ###

Zack Newsham recrutation process

### How do I get set up? ###
1. Clone repo and checkout develop branch
2. meteor npm install
3. meteor run

### How to run tests? ###
Ensure you have correct geckodriver in ./webdriverio-test folder. In repo there's macosx 0.17.0 version. To other versions please visit - https://github.com/mozilla/geckodriver/releases.

1. Selenium standalone - npm run selenium-server-start
2. Launch selenium tests - npm run selenium-tests-run

### Who do I talk to? ###

Mariusz Stefanowski <xrdj6c@gmail.com>